#region Copyright

//  
//    Copyright © 2000-2015 Workdynamics Technologies, Inc
//    CcmEnterprise 
//   
//     Repository.SharepointClient
//     George Solomon
//     12/19/2017 

#endregion

#region Imports

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using Common.Configuration;
using Common.Interfaces;
using Common.Logging;
using Common.ObjectModel;
using Constants;
using Messages.Wcf.Repository;
using Microsoft.SharePoint.Client;
using File = Microsoft.SharePoint.Client.File;
using Version = Common.ObjectModel.Version;

#endregion

namespace Sharepoint
{
    public class Client : IDocument, IDisposable
    {
        #region Instance constants

        #endregion

        #region Instance variables

        private static readonly ILog LOGGER = LogManager.GetLogger(typeof(Client));

        private readonly List<RepositoryOperations> supportedOperations = new List<RepositoryOperations>
        {
            RepositoryOperations.Add,
            RepositoryOperations.AddFileVersion,
            RepositoryOperations.Checkin,
            RepositoryOperations.Checkout,
            RepositoryOperations.Retrieve,
            RepositoryOperations.Update,
            RepositoryOperations.Delete,
            RepositoryOperations.Available,
            RepositoryOperations.Versioning,
            RepositoryOperations.UndoCheckout
        };

        private string user;
        private string password;
        private List docList;
        private String url;
        private bool ssoEnabled;

        #endregion

        public Client()
        {
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Disposed)
            {
                return;
            }
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region IDocument Members

        /// <summary>
        ///   Gets the type of the plug-in.
        /// </summary>
        /// <value>The type of the plug-in.</value>
        public RepositoryTypes PluginType => RepositoryTypes.SharePoint;

        public string Site => url;


        /// <summary>
        ///   Initializes the specified settings.
        /// </summary>
        /// <param name = "settings">The settings.</param>
        public void Initialize(RepositoryElement settings)
        {
            ConnectorElement connector = settings.Connector;
            if (connector.Credentials == null || connector.Credentials.Count <= 0)
            {
                return;
            }

            /* Check if more than 1 login information was passed, common in eDocs */
            int count = connector.Credentials.Count;
            if (count > 1)
            {
                LOGGER.ErrorFormat("Expected one entry for credentials. Found: {0}.  Only the first entry will be used.  Please check your configuration file.", count);
            }

            CredentialElement credentialElement = connector.Credentials[0];
            ssoEnabled = connector.Sso;
            user = credentialElement.User;
            password = credentialElement.Password;
            url = connector.Url.EndsWith("/") ? $"{connector.Url}{connector.Collection}" : $"{connector.Url}/{connector.Collection}";

            try
            {
                LOGGER.InfoFormat("Initializing Sharepoint Context with URL: {0}", url);
                ConfigureSharePointServices();
            }
            catch (Exception ex)
            {
                LOGGER.ErrorFormat("Tried to get Folders from : {0} but failed. {1}", url, ex.Message);
            }
        }

        /// <summary>
        ///   Determines whether this instance is available.
        /// </summary>
        /// <returns>
        ///   <c>true</c>
        ///   if this instance is available; otherwise,
        ///   <c>false</c>
        ///   .</returns>
        public bool IsAvailable()
        {
            bool retValue = true;
            using (ClientContext ctx = new ClientContext(url))
            {
                try
                {
                    ctx.Credentials = new NetworkCredential(user, password);
                    ctx.Web.Folders.GetByUrl(url);
                    LOGGER.Debug("System isAvailable");
                }
                catch (Exception ex)
                {
                    retValue = false;
                    LOGGER.DebugFormat("Tried to get Folders from : {0} but failed with exception {1}", url, ex.Message);
                }
            }

            return retValue;
        }

        /// <summary>
        /// Determine if the requested operation is supprted by this plugin
        /// </summary>
        /// <param name="operation">The requested operation</param>
        /// <returns>true if the operation is supported</returns>
        public bool IsOperationSupported(RepositoryOperations operation)
        {
            return IsAvailable() && supportedOperations.Contains(operation);
        }

        /// <summary>
        /// Authenticate the user credentials
        /// </summary>
        /// <param name="userLogon">The users credentials</param>
        /// <returns>true if authentication was successful</returns>
        public bool Login(Credentials userLogon)
        {
            if (ssoEnabled)
            {
                LOGGER.Error("SSO is not supported in this version.");
                return false;
            }

            LOGGER.InfoFormat("Logon credentials found.");
            ConfigureSharePointServices();

            return IsAvailable();
        }

        /// <summary>
        /// Checkin a document to the repository
        /// </summary>
        /// <param name="documentId">The documentId identifying the document</param>
        /// <returns>Errorcode: the result of the action</returns>
        public ErrorCodes Checkin(DocumentId documentId)
        {
            try
            {
                File file = GetDoucmentById("Documents", documentId);
                file.CheckIn(null, CheckinType.MajorCheckIn);
            }
            catch (Exception ex)
            {
                LOGGER.ErrorFormat("Unable to Checkin documentId:{0}", documentId.GetObjectId<int>());
                LOGGER.Error(ex.Message);
                return ErrorCodes.UnspecifiedError;
            }

            return ErrorCodes.Success;
        }

        /// <summary>
        /// Checkout a document from the repository
        /// </summary>
        /// <param name="documentId">documentId uniquely identifies a document</param>
        /// <returns>ErrorCode: the result of the action</returns>
        public ErrorCodes Checkout(DocumentId documentId)
        {
            try
            {
                File file = GetDoucmentById("Documents", documentId);

                file.CheckOut();
            }
            catch (Exception ex)
            {
                LOGGER.ErrorFormat("Unable to Checkout documentId: {0}", documentId.GetObjectId<int>());
                LOGGER.Error(ex.Message);
                return ErrorCodes.UnspecifiedError;
            }

            return ErrorCodes.Success;
        }

        /// <summary>
        /// UndoCheckout.  I changed my mind and don't want to checkout the document
        /// </summary>
        /// <param name="documentId">uniquely identifies the document</param>
        /// <returns>the result of the action</returns>
        public ErrorCodes UndoCheckout(DocumentId documentId)
        {
            try
            {
                File file = GetDoucmentById("Documents", documentId);

                file.UnPublish(null);
            }
            catch (Exception ex)
            {
                LOGGER.ErrorFormat("Unable to UndoCheckOut documentId:{0}", documentId.GetObjectId<int>());
                LOGGER.Error(ex.Message);
                return ErrorCodes.UnspecifiedError;
            }

            return ErrorCodes.Success;
        }

        /// <summary>
        /// Determine if the file is locked.
        /// </summary>
        /// <param name="documentId">identify the document</param>
        /// <returns>the result of the action, and the document status</returns>
        public Tuple<ErrorCodes, DocumentStatus> IsLocked(DocumentId documentId)
        {
            DocumentStatus docStatus = DocumentStatus.Unknown;
            try
            {
                File file = GetDoucmentById("Documents", documentId);

                docStatus = file.CheckedOutByUser.UserId != null
                    ? DocumentStatus.Locked
                    : DocumentStatus.Unlocked;
            }
            catch (Exception)
            {
                LOGGER.ErrorFormat("Unable to determine if documentId:{0} is locked", documentId.GetObjectId<int>());

                return new Tuple<ErrorCodes, DocumentStatus>(ErrorCodes.UnspecifiedError, docStatus);
            }

            return new Tuple<ErrorCodes, DocumentStatus>(ErrorCodes.Success, docStatus);
        }

        /// <summary>
        /// Get the latest version of the document
        /// </summary>
        /// <param name="documentId">identifies the document</param>
        /// <returns>the result of the action, and the version of the document</returns>
        public Tuple<ErrorCodes, Version> GetLatestVersion(DocumentId documentId)
        {
            Tuple<ErrorCodes, Version> retVal;
            try
            {
                File file = GetDoucmentById("Documents", documentId);
                retVal = new Tuple<ErrorCodes, Version>(ErrorCodes.Success, new Version(file.MajorVersion, file.MajorVersion, 0));
            }
            catch (Exception ex)
            {
                LOGGER.ErrorFormat("Unable to GetLatestVersion for documentId:{0}", documentId.GetObjectId<int>());
                LOGGER.Error(ex.Message);
                retVal = new Tuple<ErrorCodes, Version>(ErrorCodes.NativeRepositoryError, null);
            }

            return retVal;
        }

        /// <summary>
        /// Reads all bytes from the specified file and returns the filled buffer.
        /// </summary>
        /// <param name="documentId">The
        /// <see cref="DocumentId"/>
        /// that identifies what document must be used.</param>
        /// <returns>
        /// A byte array with the bytes read from the source if no error occur; otherwise null.
        /// </returns>
        public Tuple<ErrorCodes, byte[]> GetFileByVersion(DocumentId documentId)
        {
            using (ClientContext ctx = new ClientContext(Site))
            {
                ErrorCodes errorCode;
                byte[] buffer = null;
                try
                {
                    ctx.Credentials = new NetworkCredential(user, password);
                    File file = GetDoucmentById("Documents", documentId);

                    FileVersionCollection versions = file.Versions;
                    ctx.Load(versions.GetById(documentId.GetVersion<int>()));
                    ctx.ExecuteQuery();

                    ClientResult<Stream> stream = file.OpenBinaryStream();
                    ctx.ExecuteQuery();

                    buffer = new byte[(int) stream.Value.Length];
                    stream.Value.Read(buffer, 0, (int) stream.Value.Length);

                    errorCode = ErrorCodes.Success;
                }
                catch (Exception)
                {
                    LOGGER.ErrorFormat("Unable to GetFileByVersion for documentId:{0}", documentId.GetObjectId<int>());

                    errorCode = ErrorCodes.UnspecifiedError;
                }

                return new Tuple<ErrorCodes, byte[]>(errorCode, buffer);
            }
        }

        /// <summary>
        ///   Reads all bytes from the specified file and returns the filled buffer.
        /// </summary>
        /// <param name = "documentId">The
        ///   <see cref = "DocumentId" />
        ///   that identifies what document must be used.</param>
        /// <returns>A byte array with the bytes read from the source if no error occur; otherwise null.</returns>
        public Tuple<ErrorCodes, byte[]> GetFileByLastVersion(DocumentId documentId)
        {
            DocumentId currDocument = new DocumentId(documentId.GetObjectId<object>(), new Version());
            return GetFileByVersion(currDocument);
        }

        /// <summary>
        /// Adds the file to the repository and creates a new version.
        /// </summary>
        /// <param name="documentId">The
        /// <see cref="DocumentId"/>
        /// that identifies what document must be used.</param>
        /// <param name="documentName">The document/title name.</param>
        /// <param name="fileMetadata">The
        /// <see cref="FileMetadata"/>
        /// that identifies the file which must be added.</param>
        /// <returns>
        /// The file version is successfull, otherwise an empty string.
        /// </returns>
        /// <remarks>
        /// To create a new version sets the DocumentId.Version property to an empty string.
        /// </remarks>
        public Tuple<ErrorCodes, DocumentId> AddDocument(DocumentId documentId, string documentName, FileMetadata fileMetadata)
        {
            return AddNewDocument(documentId, documentName, fileMetadata);
        }

        /// <summary>
        /// Adds the file to the repository and creates a new version.
        /// </summary>
        /// <param name="existingDocumentId"></param>
        /// <param name="documentName">The document/title name.</param>
        /// <param name="fileMetadata">The
        /// <see cref="FileMetadata"/>
        /// that identifies the file which must be added.</param>
        /// <returns>
        /// The file version is successfull, otherwise an empty string.
        /// </returns>
        /// <remarks>
        /// To create a new version sets the DocumentId.Version property to an empty string.
        /// </remarks>
        public Tuple<ErrorCodes, DocumentId> AddNewDocument(DocumentId existingDocumentId, string documentName, FileMetadata fileMetadata)
        {
            DocumentId documentId;
            Tuple<ErrorCodes, Version> verTuple;
            bool existingDocument;
            if (existingDocumentId != null)
            {
                LOGGER.InfoFormat("Found document with documentId: {0}", existingDocumentId);
                documentId = existingDocumentId;
                verTuple = GetLatestVersion(documentId);
                existingDocument = true;
            }
            else
            {
                LOGGER.InfoFormat("AddNewDocument called with documentName:{0}", documentName);
                documentId = AddFileToSharePointList("Documents", documentName, fileMetadata, true);
                verTuple = new Tuple<ErrorCodes, Version>(ErrorCodes.Success, new Version("1", "0", "0"));
                existingDocument = false;
            }

            if (verTuple.Item1 != ErrorCodes.Success)
            {
                return new Tuple<ErrorCodes, DocumentId>(verTuple.Item1, documentId);
            }

            Version version;
            if (existingDocument)
            {
                int versionNum = documentId.GetVersion<int>() + 1;
                version = new Version(versionNum.ToString(CultureInfo.InvariantCulture), "0", "0");
            }
            else
            {
                version = verTuple.Item2;
            }

            LOGGER.Info(m => m(string.Format(CultureInfo.InvariantCulture, "Document version : {0}", version.ToString())));
            documentId.SetVersionNum(version);

            if (fileMetadata == null || !System.IO.File.Exists(fileMetadata.FileName))
            {
                return new Tuple<ErrorCodes, DocumentId>(ErrorCodes.FileNotFound, documentId);
            }

            return new Tuple<ErrorCodes, DocumentId>(ErrorCodes.Success, documentId);
        }

        public Tuple<ErrorCodes, DocumentId> AddNewDocument(DocumentId existingDocumentId, string documentName, FileMetadata fileMetadata, Collection<ProfileField> fields,
            Collection<Trustee> trustees)
        {
            return AddNewDocument(existingDocumentId, documentName, fileMetadata);
        }

        /// <summary>
        /// Get the Enterprise to Repository field mapping
        /// </summary>
        /// <param name="documentId">identifies the document</param>
        /// <param name="fields">the fields to be mapped</param>
        /// <returns>the result of the action, a dictionary of field and value</returns>
        public Tuple<ErrorCodes, Dictionary<string, string>> GetValueForProfiles(DocumentId documentId, IEnumerable<string> fields)
        {
            return IsAvailable()
                ? new Tuple<ErrorCodes, Dictionary<string, string>>(ErrorCodes.Success, new Dictionary<string, string>())
                : new Tuple<ErrorCodes, Dictionary<string, string>>(ErrorCodes.NotResponding, null);
        }

        /// <summary>
        /// Update the Enterprise / Repository field mappings
        /// </summary>
        /// <param name="documentId">identifies the document</param>
        /// <param name="fields">the fields to be mapped</param>
        /// <returns>the result of the action</returns>
        public ErrorCodes UpdateValueForProfiles(DocumentId documentId, IEnumerable<ProfileField> fields)
        {
            return IsAvailable() ? ErrorCodes.Success : ErrorCodes.NotResponding;
        }

        /// <summary>
        /// Get the version history of the document
        /// </summary>
        /// <param name="documentId">identifies the document</param>
        /// <returns>the result of the action, a list of the versions</returns>
        public Tuple<ErrorCodes, List<EdocsHistory>> GetVersionHistory(DocumentId documentId)
        {
            List<EdocsHistory> history = new List<EdocsHistory>();
            using (ClientContext ctx = new ClientContext(url))
            {
                ctx.Credentials = new NetworkCredential(user, password);
                File file = GetDoucmentById("Documents", documentId);
                FileVersionCollection versions = file.Versions;
                ctx.Load(versions.GetById(documentId.GetVersion<int>()));
                ctx.ExecuteQuery();

                history.AddRange(versions.Select(fileVersion => new EdocsHistory
                {
                    Author = fileVersion.CreatedBy.UserId.NameId,
                    Date = fileVersion.Created.Date.ToShortDateString(),
                    Description = fileVersion.CheckInComment,
                    Number = fileVersion.VersionLabel
                }));
            }
            return new Tuple<ErrorCodes, List<EdocsHistory>>(ErrorCodes.Success, history);
        }

        /// <summary>
        /// Do a full text search on teh respository
        /// </summary>
        /// <param name="text">the text to search for</param>
        /// <returns>the result of the action, and a dictionary of the terms and their score</returns>
        public Tuple<ErrorCodes, Dictionary<string, string>> FullTextSearch(string text)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Destructors

        private bool Disposed { get; set; }

        ~Client()
        {
            Dispose(false);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }
            url = null;
            Disposed = true;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Configures the important Sharepoint URLS
        /// </summary>
        private void ConfigureSharePointServices()
        {
            LOGGER.Info(m => m(string.Format(CultureInfo.InvariantCulture, "SharePoint Web Service {0}", url)));

            using (ClientContext ctx = new ClientContext(url))
            {
                try
                {
                    ctx.Credentials = new NetworkCredential(user, password);
                    docList = ctx.Web.Lists.GetByTitle("Documents"); //get the Documents List
                }
                catch (Exception ex)
                {
                    LOGGER.ErrorFormat("Unable to ConfigureSharepointServices");
                    LOGGER.ErrorFormat(ex.Message);
                }
            }
        }

        /// <summary>
        /// Adds a file resource to the repository
        /// </summary>
        /// <param name="list">the Sharepoint List</param>
        /// <param name="title">the fullpath to the file</param>
        /// <param name="fileMetaData"></param>
        /// <param name="overwrite">overwrite existing file</param>
        /// <returns>the identifier for the docuemnt</returns>
        private DocumentId AddFileToSharePointList(string list, string title, FileMetadata fileMetaData, bool overwrite)
        {
            LOGGER.InfoFormat("Adding file: {0} to sharepoint list: {1} with overwrite: {2}", title, list, overwrite);

            /* Read the File */
            byte[] fileBytes = null;
            try
            {
                LOGGER.InfoFormat("Reading file: {0}", fileMetaData.FileName);

                FileStream fs = new FileStream(fileMetaData.FileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                long numBytes = new FileInfo(fileMetaData.FileName).Length;
                fileBytes = br.ReadBytes((int) numBytes);
            }
            catch (Exception e)
            {
                LOGGER.ErrorFormat("Unable to read file: {0}", fileMetaData.FileName);
                LOGGER.Error(e.Message);
            }

            DocumentId documentId;
            using (ClientContext ctx = new ClientContext(Site))
            {
                try
                {
                    docList = ctx.Web.Lists.GetByTitle(list);
                    Folder folder = docList.RootFolder;

                    var file = new FileCreationInformation
                    {
                        Url = title,
                        Content = fileBytes,
                        Overwrite = true
                    };

                    var uploadFile = folder.Files.Add(file);

                    ctx.Load(uploadFile,
                        f => f.ListItemAllFields,
                        f => f.ListItemAllFields.Id,
                        f => f.MajorVersion,
                        f => f.MinorVersion);

                    ctx.ExecuteQuery();
                    documentId = new DocumentId(uploadFile.ListItemAllFields.Id, uploadFile.MajorVersion, uploadFile.MinorVersion, 0);
                    LOGGER.DebugFormat("Created file with path:{0} with id:{1}", fileMetaData.FileName, uploadFile.ListItemAllFields.Id);
                }
                catch (Exception e)
                {
                    LOGGER.ErrorFormat("Unable to AddFileToSharePointList:{0} for file:{1}", list, fileMetaData.FileName);
                    LOGGER.Error(e.Message);
                    documentId = new DocumentId {ErrorMessage = "Failed to add file."};
                }
            }

            return documentId;
        }

        private File GetDoucmentById(string list, DocumentId documentId)
        {
            using (ClientContext ctx = new ClientContext(url))
            {
                try
                {
                    ctx.Credentials = new NetworkCredential(user, password);
                    LOGGER.InfoFormat("Finding documentId:{0} in Sharepoint List: {1}", documentId.GetObjectId<int>(), list);
                    docList = ctx.Web.Lists.GetByTitle(list); // fetch the list
                    ListItem item = docList.GetItemById(documentId.GetObjectId<int>());
                    ctx.Load(item, i => i.File);
                    ctx.ExecuteQuery();
                    return item.File;
                }
                catch (Exception ex)
                {
                    LOGGER.ErrorFormat("Unable to getDocumentById:{0}", documentId.GetObjectId<int>());
                    LOGGER.Error(ex.Message);
                    return null;
                }
            }
        }

        #endregion
    }
}